/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Scanner;

/**
 *
 * @author wasuk
 */
public class Tools {
    private static Scanner input = new Scanner(System.in);
    
    public static int getAnInt(String inputMsg, String errMsg) {
        int number = 0;
        boolean vaildNumber;
        do {
            try {
                System.out.print(inputMsg + ": ");
                number = Integer.parseInt(input.nextLine());
                vaildNumber = true;
            } catch (Exception e) {
                vaildNumber = false;
                System.err.println(errMsg);
            }
        } while(vaildNumber != true);
        return number;
    }
    
    public static int getAnInt(String inputMsg, String errMsg, int lower, int upper) {
        int number = 0;
        if(lower > upper) {
            int temp = lower;
            lower = upper;
            upper = lower;
        }
        boolean vaildNumber;
        do {
            try {
                System.out.print(inputMsg + ": ");
                number = Integer.parseInt(input.nextLine());
                if(number < lower || number > upper) {
                    throw new ArithmeticException();
                }
                vaildNumber = true;
            } catch (Exception e) {
                vaildNumber = false;
                System.err.println(errMsg);
            }
        } while(vaildNumber != true);
        return number;
    }
    
    public static double getAnDouble(String inputMsg, String errMsg) {
        double number = 0;
        boolean vaildNumber;
        do {
            try {
                System.out.print(inputMsg + ": ");
                number = Double.parseDouble(input.nextLine());
                vaildNumber = true;
            } catch (Exception e) {
                vaildNumber = false;
                System.err.println(errMsg);
            }
        } while(vaildNumber != true);
        return number;
    }
    
    public static double getAnDouble(String inputMsg, String errMsg, double lower, double upper) {
        double number = 0;
        if(lower > upper) {
            double temp = lower;
            lower = upper;
            upper = lower;
        }
        boolean vaildNumber;
        do {
            try {
                System.out.print(inputMsg + ": ");
                number = Double.parseDouble(input.nextLine());
                if(number < lower || number > upper) {
                    throw new ArithmeticException();
                }
                vaildNumber = true;
            } catch (Exception e) {
                vaildNumber = false;
                System.err.println(errMsg);
            }
        } while(vaildNumber != true);
        return number;
    }
    
    public static String getString(String inputMsg, String errMsg, String pattern, int maxSize) {
        String string;
        boolean vaildString = false;
        do {
            System.out.print(inputMsg + ": ");
            string = input.nextLine();
            if(string.length() > maxSize) {
                vaildString = false;
                System.out.println("Max character is " + maxSize);
            } else {
                vaildString = string.matches(pattern);
                if(vaildString == false)
                    System.err.println(errMsg);
            }
        } while(vaildString != true);
        string = removeWhiteSpace(string);
        return string;
    }
    
    public static String getString(String inputMsg, String errMsg, String pattern, int lowSize, int maxSize) {
        String string;
        boolean vaildString = false;
        do {
            System.out.print(inputMsg + ": ");
            string = input.nextLine();
            if(string.length() < lowSize || string.length() > maxSize) {
                vaildString = false;
                System.err.println(errMsg);
            } else {
                vaildString = string.matches(pattern);
                if(vaildString == false)
                    System.err.println(errMsg);
            }
        } while(vaildString != true);
        string = removeWhiteSpace(string);
        return string;
    }
    
    public static String removeWhiteSpace(String string) {
        while(string.matches(".*\\s{2,}.*")) {
            string = string.replaceAll("\\s{2}", " ");
        }
        return string.trim();
    }
    
    public static char getAChar(String inputMsg, String pattern, String errMsg) {
        String answer;
        char character;
        do {
            System.out.print(inputMsg + ": ");
            answer = input.nextLine();
            if(answer.isEmpty() == false) {
                character = answer.charAt(0);
                for(int i = 0; i < pattern.length(); i++) {
                    if(pattern.charAt(i) == character) 
                        return character;
                }
            } 
            System.out.println(errMsg);
        } while(true);
    }
    
    public static void main(String[] args) {
        //System.out.println(getString("- Input name", "Please input name","[a-zA-Z][^0-9]*", 20));
        //System.out.println("abc_123@fpt.edu.com".matches("[\\w]+@[[\\w]+\\.[\\w]+]+"));
        System.out.println(getString("- Address", "* No", "(\\w+|\\s|_|\\\\|\\.|-|,)+", 50));
        
    }
}
