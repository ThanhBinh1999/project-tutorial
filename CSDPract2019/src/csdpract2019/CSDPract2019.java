/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csdpract2019;

import manage.StaffManagement;
import menu.Menu;

/**
 *
 * @author wasuk
 */
public class CSDPract2019 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Menu menu = new Menu("--- Staff Managenment ---");
        menu.addItem("Add new staff");
        menu.addItem("Search a staff by email");
        menu.addItem("List all staffs");
        menu.addItem("Remove a staff by email");
        menu.addItem("Exit");
        StaffManagement sm = new StaffManagement();
        int choice;
        do {
            menu.showMenu();
            choice = menu.getChoice();
            switch(choice) {
                case 1: sm.addNewStaff();
                        break;
                case 2: sm.searchStaffByEmail();
                        break;
                case 3: sm.printAllStaff();
                        break;
                case 4: sm.removeStaffByEmail();
                        break;
            }
        } while(choice != 5);
    }
    
}
