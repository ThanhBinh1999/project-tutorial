/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manage;

import data.Staff;
import java.util.TreeSet;
import util.Tools;

/**
 *
 * @author wasuk
 */
public class StaffManagement extends TreeSet<Staff> {
    
    public void addNewStaff() {
        String email, address, department;
        int level, salary;
        boolean exist = false;
        System.out.println("--- Add New Staff ---");
        do {
            if(exist)
                System.err.println("* Error: Email exist!");
            email = Tools.getString(" - Email", "* Invaild Email Format (abc@gmail.com).", "[\\w]+@[[\\w]+\\.[\\w]+]+", 50);
            exist = isExist(email);
        } while(exist);
        address = Tools.getString(" - Address", "* Please enter vaild address!", "(\\w+|\\s|_|\\\\|\\.|-|,)+", 50);
        level = Tools.getAnInt(" - Level", "* Level must be positive integer and greater than 0!", 1, 100);
        salary = Tools.getAnInt(" - Salary", "* Salary must be positive integer and greater than 0!", 1, 999999999);
        department = Tools.getString(" - Department", "* Please enter vaild department!", "\\w+", 15);
        if(this.add(new Staff(email, address, department, level, salary)))
            System.out.println("* Add New Staff Successfully!");
        else
            System.err.println("* Add New Staff Failed!");
    }
    
    public void searchStaffByEmail() {
        if(this.isEmpty()) {
            System.err.println("* List is empty!");
            return;
        }
        String email;
        System.out.println("--- Search Staff By Email ---");
        email = Tools.getString(" - Email", "* Invaild Email Format (abc@gmail.com).", "[\\w]+@[[\\w]+\\.[\\w]+]+", 50);
        if(isExist(email)) {
            Staff getStaff = this.floor(new Staff(email));
            System.out.println("- Staff Found: " + getStaff);
        } else
            System.err.println("* Staff Not Found!");
    }
    
    public void printAllStaff() {
        if(this.isEmpty()) {
            System.err.println("* List is empty!");
            return;
        }
        System.out.println("--- Print All Staff ---");
        for (Staff staff : this) {
            System.out.println("    " + staff);
        }
    }
    
    public void removeStaffByEmail() {
        if(this.isEmpty()) {
            System.err.println("* List is empty!");
            return;
        }
        String email;
        System.out.println("--- Remove Staff By Email ---");
        email = Tools.getString(" - Email", "* Invaild Email Format (abc@gmail.com).", "[\\w]+@[[\\w]+\\.[\\w]+]+", 50);
        if(isExist(email)) {
            if(this.remove(new Staff(email)))
                System.out.println("* Remove staff email " + email + " successfully!");
            else
                System.err.println("* Remove failed!");
        } else
            System.err.println("* Staff Not Found!");
    }
    
    private boolean isExist(String email) {
        return this.contains(new Staff(email));
    }
}
