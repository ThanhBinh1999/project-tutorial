/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author wasuk
 */
public class Staff implements Comparable<Staff> {
    private String email, address, department;
    private int level, salary;

    public Staff() {
    }

    public Staff(String email) {
        this.email = email;
    }

    public Staff(String email, String address, String department, int level, int salary) {
        this.email = email;
        this.address = address;
        this.department = department;
        this.level = level;
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return email + " - " + address + " - " + department + " - " + level + " - " + salary + '$';
    }
    
    @Override
    public int compareTo(Staff o) {
        return email.compareTo(o.email);
    }
    
    
}
