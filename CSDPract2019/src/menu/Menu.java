/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.ArrayList;
import util.Tools;

/**
 *
 * @author wasuk
 */
public class Menu {
    private String title, item;
    private ArrayList<String> menu = new ArrayList<>();
     
    public Menu(String title) {
        this.title = title;
    }
    
    public void addItem(String item) {
        menu.add(item);
    }
    
    public int getChoice() {
        return Tools.getAnInt("> Please Choice", "* Please enter An Integer Between 1 and " + menu.size(), 1, menu.size());
    }
    
    public void showMenu() {
        System.out.println("");
        System.out.println(title);
        for (int i = 0; i < menu.size(); i++) {
            System.out.printf("%d. %s\n", (i+1), menu.get(i));
        }
    }
}
