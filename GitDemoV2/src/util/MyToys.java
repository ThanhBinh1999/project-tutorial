/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author wasuk
 */
public class MyToys {

    public static int computeFactorial(int n) {
//        if (n < 0) {
//            throw new IllegalArgumentException("Invalid argment, It must be >= 0");
//        } else if (n == 0 || n == 1) {
//            return 1;
//        } else {
//            return computeFactorial(n - 1) * n;
//        }
        int tmp = 1;
        if (n < 0) {
            throw new IllegalArgumentException("Invalid input. It must be >= 0");
        }
        if (n == 0 || n == 1) {
            return 1;
        } else {
            for (int i = 1; i <= n; i++) {
                tmp *= i;//tmp = tmp * i; //kỹ thuật nhồi heo đất
            }
            return tmp;
        }
    }
}
