/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitdemov2;

import util.MyToys;

/**
 *
 * @author wasuk
 */
public class GitDemoV2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("5! = " + MyToys.computeFactorial(5));
        System.out.println("6! = " + MyToys.computeFactorial(6));
        System.out.println("0! = " + MyToys.computeFactorial(0));
        //vi diệu
        System.out.println("-5! = " + MyToys.computeFactorial(-5));
    }

}
