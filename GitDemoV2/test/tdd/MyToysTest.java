/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdd;

import org.junit.Test;
import static org.junit.Assert.*;
import static util.MyToys.computeFactorial;

/**
 *
 * @author wasuk
 */
public class MyToysTest {

    @Test
    //Từ khóa này là anotation giúp biến 1 hàm bất kì thành main
    public void testSuccessCases() {
        //Test tình huốn đưa vào giai thừa đúng
        assertEquals(120, computeFactorial(5));
        assertEquals(1, computeFactorial(0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFailCases() {
        //Test tình huốn đưa vào giai thừa đúng
        computeFactorial(-5);

    }    

}
